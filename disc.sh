#!/bin/sh

#
# script to discover constants and macros from OpenSHMEM installation
# (if macro -> no symbol -> Go can't detect it directly)
#

#
# make sure we get OSHMEM
#
module rm osss-ucx/ucx/gcc8/git/pmix/3.2.3
module add openmpi/gcc8/4.1.2

c_source=`mktemp -t -u booXXXXXX.c`

cat > $c_source <<EOF
#include <stdio.h>
#include <shmem.h>

int
main()
{
EOF

while read const fmt
do
    gosym=`echo $const | sed -e 's/^SHMEM_//'`
    # need to quote strings
    if [ $fmt == '%s' ]
    then
        fmt="\\\"$fmt\\\""
    fi
    cat >> $c_source <<EOF
    printf("const %-32s = $fmt\n", "$gosym", $const);
EOF
done < discover.txt

cat >> $c_source <<EOF
    return 0;
}
EOF

oshcc $c_source

./a.out

rm -f $c_source a.out
